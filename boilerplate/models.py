from webapp2_extras.appengine.auth.models import User
from google.appengine.api import search
from google.appengine.ext import ndb
from google.appengine.ext import db

import logging

from argus_api_messages import IncidentResponseMessage
from argus_api_messages import GroupResponseMessage

import datetime
# import pytz


class User(User):
    """
    Universal user model. Can be used with App Engine's default users API,
    own auth or third party authentication methods (OpenID, OAuth etc).
    based on https://gist.github.com/kylefinley
    """

    #: Creation date.
    created = ndb.DateTimeProperty(auto_now_add=True)
    #: Modification date.
    updated = ndb.DateTimeProperty(auto_now=True)
    #: User defined unique name, also used as key_name.
    # Not used by OpenID
    username = ndb.StringProperty()
    #: User Name
    name = ndb.StringProperty()
    #: User Last Name
    last_name = ndb.StringProperty()
    #: User email
    email = ndb.StringProperty()
    #: Hashed password. Only set for own authentication.
    # Not required because third party authentication
    # doesn't use password.
    password = ndb.StringProperty()
    #: User Country
    country = ndb.StringProperty()
    #: User TimeZone
    tz = ndb.StringProperty()
    #: Account activation verifies email
    activated = ndb.BooleanProperty(default=False)

    @classmethod
    def get_by_email(cls, email):
        """Returns a user object based on an email.

        :param email:
            String representing the user email. Examples:

        :returns:
            A user object.
        """
        return cls.query(cls.email == email).get()

    @classmethod
    def create_resend_token(cls, user_id):
        entity = cls.token_model.create(user_id, 'resend-activation-mail')
        return entity.token

    @classmethod
    def validate_resend_token(cls, user_id, token):
        return cls.validate_token(user_id, 'resend-activation-mail', token)

    @classmethod
    def delete_resend_token(cls, user_id, token):
        cls.token_model.get_key(user_id, 'resend-activation-mail', token).delete()

    def get_social_providers_names(self):
        social_user_objects = SocialUser.get_by_user(self.key)
        result = []
#        import logging
        for social_user_object in social_user_objects:
#            logging.error(social_user_object.extra_data['screen_name'])
            result.append(social_user_object.provider)
        return result

    def get_social_providers_info(self):
        providers = self.get_social_providers_names()
        result = {'used': [], 'unused': []}
        for k,v in SocialUser.PROVIDERS_INFO.items():
            if k in providers:
                result['used'].append(v)
            else:
                result['unused'].append(v)
        return result


class LogVisit(ndb.Model):
    user = ndb.KeyProperty(kind=User)
    uastring = ndb.StringProperty()
    ip = ndb.StringProperty()
    timestamp = ndb.StringProperty()


class LogEmail(ndb.Model):
    sender = ndb.StringProperty(
        required=True)
    to = ndb.StringProperty(
        required=True)
    subject = ndb.StringProperty(
        required=True)
    body = ndb.TextProperty()
    when = ndb.DateTimeProperty()


class SocialUser(ndb.Model):
    PROVIDERS_INFO = { # uri is for OpenID only (not OAuth)
        'google': {'name': 'google', 'label': 'Google', 'uri': 'gmail.com'},
        'github': {'name': 'github', 'label': 'Github', 'uri': ''},
        'facebook': {'name': 'facebook', 'label': 'Facebook', 'uri': ''},
        'linkedin': {'name': 'linkedin', 'label': 'LinkedIn', 'uri': ''},
        'myopenid': {'name': 'myopenid', 'label': 'MyOpenid', 'uri': 'myopenid.com'},
        'twitter': {'name': 'twitter', 'label': 'Twitter', 'uri': ''},
        'yahoo': {'name': 'yahoo', 'label': 'Yahoo!', 'uri': 'yahoo.com'},
    }

    user = ndb.KeyProperty(kind=User)
    provider = ndb.StringProperty()
    uid = ndb.StringProperty()
    extra_data = ndb.JsonProperty()

    @classmethod
    def get_by_user(cls, user):
        return cls.query(cls.user == user).fetch()

    @classmethod
    def get_by_user_and_provider(cls, user, provider):
        return cls.query(cls.user == user, cls.provider == provider).get()

    @classmethod
    def get_by_provider_and_uid(cls, provider, uid):
        return cls.query(cls.provider == provider, cls.uid == uid).get()

    @classmethod
    def check_unique_uid(cls, provider, uid):
        # pair (provider, uid) should be unique
        test_unique_provider = cls.get_by_provider_and_uid(provider, uid)
        if test_unique_provider is not None:
            return False
        else:
            return True

    @classmethod
    def check_unique_user(cls, provider, user):
        # pair (user, provider) should be unique
        test_unique_user = cls.get_by_user_and_provider(user, provider)
        if test_unique_user is not None:
            return False
        else:
            return True

    @classmethod
    def check_unique(cls, user, provider, uid):
        # pair (provider, uid) should be unique and pair (user, provider) should be unique
        return cls.check_unique_uid(provider, uid) and cls.check_unique_user(provider, user)

    @staticmethod
    def open_id_providers():
        return [k for k,v in SocialUser.PROVIDERS_INFO.items() if v['uri']]

class Incident(db.Model):
  """ Model for incidents reported.
  """
  incident_type = db.StringProperty(required=True)
  created = db.DateTimeProperty(auto_now_add=True)
  incident_date = db.DateTimeProperty()
  location = db.GeoPtProperty(required=True)
  description = db.TextProperty()

  # TODO: log incident reporter...

  def put(self, *args, **kwargs):
    # TODO: Readd this later. See note in docs.
    # if self.incident_date > datetime.datetime.today():
    #  raise ValueError('Incident date is in the future')

    return super(Incident, self).put(*args, **kwargs)

  def to_message(self):
    return IncidentResponseMessage(id=self.key().id(),
                                   incident_type=self.incident_type,
                                   incident_date=str(self.incident_date),
                                   location=str(self.location),
                                   description=self.description)

  @classmethod
  def put_from_message(cls, message):
    test = message.location.split()
    # print test
    blah = test[0].split(',')
    # print type(blah)
    # print blah[0]
    geopoint = search.GeoPoint(float(blah[0]), float(blah[1]))
    logging.info(type(message.location))
    incident = cls(incident_type = message.incident_type,
                   incident_date = message.incident_date,
                   location = ndb.GeoPt(float(blah[0]), float(blah[1])),
                   description = message.description)
    incident.put()
    return incident

class Group(db.Model):
  """ Model for groups.

  Stores a list of member IDs and incident IDs in a list.
  """
  members = db.StringListProperty()
  incidents = db.StringListProperty()
  name = db.StringProperty()
  admin = db.StringProperty()

  @classmethod
  def get_by_user_id(cls, user_id):
    usid = str(user_id)
    query = cls.all().filter('members =', usid)
    return query.run()

  def to_message(self):
    # Convert incident and member keys to strings.
    incident_key_string = ', '.join(self.incidents)
    member_key_string = ', '.join(self.members)

    return GroupResponseMessage(id=self.key().id(),
                                group_name=self.name,
                                members=member_key_string,
                                incidents=incident_key_string)
