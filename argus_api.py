"""Hello World API implemented using Google Cloud Endpoints.

Defined here are the ProtoRPC messages needed to define Schemas for methods
as well as those methods defined in an API.
"""


import endpoints
import webapp2
from protorpc import messages
from protorpc import message_types
from protorpc import remote

from boilerplate.models import Group
from boilerplate.models import User
from web.argus_models import Incident

# Import Group related messages.
from argus_api_messages import GroupAddRequest
from argus_api_messages import GroupCreateRequest
from argus_api_messages import GroupResponseMessage

# Import Incident related messages.
from argus_api_messages import IncidentResponseMessage
from argus_api_messages import IncidentListResponse
from argus_api_messages import IncidentPostMessage
from argus_api_messages import ShareIncidentToGroupsRequest


package = 'Hello'


@endpoints.api(name='argussapp', version='v2')
class ArgusAppApi(remote.Service):
    """Argus API v2."""

    @endpoints.method(IncidentResponseMessage, IncidentResponseMessage,
                      path='incidents/{id}', http_method='GET',
                      name='incidents.getIncident')
    def incident_get(self, request):
        incident = Incident.get_by_id(request.id)
        return incident.to_message()

    @endpoints.method(IncidentListResponse, IncidentListResponse,
                      path='incidents', http_method='GET',
                      name='incidents.listIncident')
    def incident_list(self, request):
      incidents = [incident.to_message() for incident in
                   Incident.all().fetch(10)]
      return IncidentListResponse(incidents=incidents)

    @endpoints.method(IncidentPostMessage, IncidentResponseMessage,
                      path='incidents', http_method='POST',
                      name='incidents.postIncident')
    def incident_post(self, request):
      incident = Incident.put_from_message(request)
      return incident.to_message()

    @endpoints.method(ShareIncidentToGroupsRequest, IncidentResponseMessage,
                      path='incidents/share/{incident_id}/{user_id}',
                      http_method='POST',
                      name='incidents.shareIncident')
    def incident_share(self, request):
      incident = Incident.get_by_id(request.incident_id)
      if incident:
        groups = Group.get_by_user_id(request.user_id)
        if groups:
          for group in groups:
            group.incidents.append(str(request.incident_id))
            group.put()
          return incident.to_message()
        else:
          raise endpoints.NotFoundException('User with ID %s not in any groups'
                                             % request.incident_id)
          return
      else:
        raise endpoints.NotFoundException('Incident with ID %s not found'
                                          % request.incident_id)
        return

    @endpoints.method(GroupCreateRequest, GroupResponseMessage,
                      path='groups/create/{group_name}',
                      http_method='POST',
                      name='groups.create')
    def create_group(self, request):
      group = Group(name=request.group_name,
                    admin=str(request.user_id))
      group.members.append(str(request.user_id))
      group.put()
      return group.to_message()

    @endpoints.method(GroupAddRequest, GroupResponseMessage,
                      path='groups/add/{group_id}/{user_id}/{new_user_id}',
                      http_method='POST',
                      name='groups.add')
    def add_to_group(self, request):
      user_id = str(request.user_id)
      new_user_id = str(request.new_user_id)
      group = Group.get_by_id(request.group_id)
      if not group:
        raise endpoints.NotFoundException(
          'Could not find group with ID %s.' % request.group_id)
        return

      if not (user_id in group.members):
        raise endpoints.ForbiddenException(
          'User with ID %s cannot add users to this group' % user_id)
        return

      if not (group.admin == user_id):
        raise endpoints.ForbiddenException(
          'User with ID %s cannot add users to this group' % user_id)
        return

      user = User.get_by_id(request.new_user_id)
      if not user:
        raise endpoints.NotFoundException(
          'Could not find user with ID %s.' % request.new_user_id)
        return

      if (new_user_id in group.members):
        raise endpoints.BadRequestException(
          'User with ID %s is already a member of the group with ID %s'
          % (new_user_id, request.group_id))
        return

      group.members.append(new_user_id)
      group.put()
      return group.to_message()

APPLICATION = endpoints.api_server([ArgusAppApi])
