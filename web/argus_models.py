from webapp2_extras.appengine.auth.models import User
from google.appengine.api import search
from google.appengine.ext import ndb
from google.appengine.ext import db

import logging

from argus_api_messages import IncidentResponseMessage
from argus_api_messages import GroupResponseMessage

import datetime
# import pytz


class Incident(db.Model):
  """ Model for incidents reported.
  """
  incident_type = db.StringProperty(required=True)
  created = db.DateTimeProperty(auto_now_add=True)
  incident_date = db.DateTimeProperty()
  location = db.GeoPtProperty(required=True)
  description = db.TextProperty()

  # TODO: log incident reporter...

  def put(self, *args, **kwargs):
    # TODO: Add incident date validation to make sure date is not in the
    # future..
    return super(Incident, self).put(*args, **kwargs)

  def to_message(self):
    return IncidentResponseMessage(id=self.key().id(),
                                   incident_type=self.incident_type,
                                   incident_date=str(self.incident_date),
                                   location=str(self.location),
                                   description=self.description)

  @classmethod
  def put_from_message(cls, message):
    input_location = message.location.split()
    split_location = input_location[0].split(',')
    geopoint = search.GeoPoint(float(split_location[0]),
                               float(split_location[1]))
    logging.info(type(message.location))
    incident = cls(incident_type = message.incident_type,
                   incident_date = message.incident_date,
                   location = ndb.GeoPt(float(split_location[0]),
                                        float(split_location[1])),
                   description = message.description)
    incident.put()
    return incident

class Group(db.Model):
  """ Model for groups. """
  members = db.StringListProperty()
  incidents = db.StringListProperty()
  name = db.StringProperty()
  admin = db.StringProperty()

  @classmethod
  def get_by_user_id(cls, user_id):
    """Function to get a list of groups a user is in."""
    usid = str(user_id)
    query = cls.all().filter('members =', usid)
    return query.run()

  def to_message(self):
    # Convert incident and member keys to strings.
    incident_key_string = ', '.join(self.incidents)
    member_key_string = ', '.join(self.members)

    return GroupResponseMessage(id=self.key().id(),
                                group_name=self.name,
                                members=member_key_string,
                                incidents=incident_key_string)
