"""ProtoRPC message class definitions for Argus API."""


from protorpc import messages
from protorpc import message_types


class GroupAddRequest(messages.Message):
  """ProtoRPC message definition to represent adding a user to a group."""
  group_id = messages.IntegerField(1, required=True)
  user_id = messages.IntegerField(2, required=True)
  new_user_id = messages.IntegerField(3, required=True)


class GroupCreateRequest(messages.Message):
  """ProtoRPC message definition to represent a group create request."""
  group_name = messages.StringField(1, required=True)
  user_id = messages.IntegerField(2, required=True)


class GroupResponseMessage(messages.Message):
  """ProtoRPC message definition to represent a group that is stored."""
  id = messages.IntegerField(1)
  group_name = messages.StringField(2)
  members = messages.StringField(3)
  incidents = messages.StringField(4)


class IncidentListRequest(messages.Message):
  """ProtoRPC message definition to represent an incident list query."""
  limit = messages.IntegerField(1, default=10)
  class Order(messages.Enum):
      WHEN = 1
      TEXT = 2
  order = messages.EnumField(Order, 2, default=Order.WHEN)


class IncidentPostMessage(messages.Message):
  """ProtoRPC message definition to represent an incident to be inserted."""
  incident_type = messages.StringField(1, required=True)
  incident_date = message_types.DateTimeField(2)
  location = messages.StringField(3, required=True)
  description = messages.StringField(4)


class IncidentRequestMessage(messages.Message):
  """ProtoRPC message definition to represent an incident to be inserted."""
  outcome = messages.StringField(1, required=True)


class ShareIncidentToGroupsRequest(messages.Message):
  incident_id = messages.IntegerField(1, required=True)
  user_id = messages.IntegerField(2, required=True)

class IncidentResponseMessage(messages.Message):
  """ProtoRPC message definition to represent an incident that is stored."""
  id = messages.IntegerField(1)
  incident_type = messages.StringField(2)
  incident_date = messages.StringField(3)
  location = messages.StringField(4)
  description = messages.StringField(5)


class IncidentListResponse(messages.Message):
  """ProtoRPC message definition to represent a list of stored incidents."""
  incidents = messages.MessageField(IncidentResponseMessage, 1, repeated=True)


class UserSignUpMessage(messages.Message):
  """ProtoRPC message definition to represent a sign up request."""
  username = messages.StringField(1, required=True)
  email_address = messages.StringField(2, required=True)
  password = messages.StringField(3, required=True)
